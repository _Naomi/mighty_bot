const fs = require('fs');

const mineflayer = require('mineflayer');
const onChange = require('on-change');
require('./lib/logger.js');

const login = require('./data/login.json');
const options = fs.existsSync('./data/options.json') ? require('./data/options.json') : {disabled: []};

if (process.argv.length < 3 && login.host == undefined) {
  throw new Error('There is no server specified, either via command line arguments or in login.json');
}

const address = process.argv.length >= 3 ? process.argv[2] : login.host + ':' + (login.port != undefined ? login.port : 25565);

/**
 * Delete all the plugins from the cache.
 */
function deletePlugins() {
  log.debug('Deleting all cached plugins.');
  fs.readdirSync('./plugins').forEach((file) => {
    delete require.cache[require.resolve('./plugins/' + file)];
  });
}

/**
 * Create the bot.
 */
function createBot() {
  log.info('Creating new bot.');
  const bot = mineflayer.createBot({
    host: address.includes(':') ? address.slice(0, address.indexOf(':')) : address,
    port: address.includes(':') ? parseInt(address.slice(address.indexOf(':')+1)) : undefined,
    username: login.username,
    password: login.password,
    auth: login.auth,
    version: options.version,
    disableChatSigning: true,
  });

  bot.once('spawn', () => {
    log.info('Successfully logged into the server!');

    bot.options = onChange(options, (path, value, previousValue) => {
      fs.writeFileSync('./data/options.json', JSON.stringify(bot.options, null, 4));
    });

    // Add normal chat patterns
    if (bot.options.chatPatterns !== undefined) {
      bot.options.chatPatterns.forEach((pattern) => bot.addChatPattern('chat', new RegExp(pattern), {parse: true}));
    }

    // Add whisper chat patterns
    if (bot.options.whisperPatterns !== undefined) {
      bot.options.whisperPatterns.forEach((pattern) => bot.addChatPattern('whisper', new RegExp(pattern), {parse: true}));
    }

    fs.readdirSync('./plugins').forEach((file) => {
      if (!bot.options.disabled.includes(file.slice(0, file.lastIndexOf('.')))) {
        const plugin = require('./plugins/' + file);
        if (plugin.inject != null) {
          if (plugin.options != null) {
            Object.keys(plugin.options).forEach((key) => {
              if (!(key in bot.options)) {
                bot.options[key] = plugin.options[key];
              }
            });
          }
          plugin.inject(bot);
        } else {
          log.warn('Plugin ' + file + ' doesn\'t have an inject function!');
        }
      }
    });
  });

  bot.on('error', (error) => {
    log.error(error.stack);
  });

  bot.on('kicked', (reason) => log.warn(`Got kicked for: ${JSON.parse(reason).text}`));

  bot.on('end', () => {
    log.error('Lost connection to the server. Connecting to server in 60 seconds.');
    deletePlugins();
    setTimeout(createBot, 60000);
  });
}

createBot();
