const fs = require('fs');
const path = require('path');

module.exports.inject = inject;

/**
 * Inject this plugin into the bot.
 *
 * @param {Bot} bot - The bot to inject into.
 */
function inject(bot) {
  const seenPlayersFile = path.join(__dirname, '../data/seenPlayers.json');
  const seenPlayers = fs.existsSync(seenPlayersFile) ? require(seenPlayersFile) : [];

  bot.on('playerJoined', (player) => {
    if (!seenPlayers.includes(player.uuid)) {
      bot.chat(`Welcome to the server, ${player.username}!`);
      seenPlayers.push(player.uuid);
      fs.writeFile(seenPlayersFile, JSON.stringify(seenPlayers, null, 4), (err) => {
        if (err) {
          log.error(err);
          throw err;
        }
      });
    }
  });
};
