const https = require('https');

const registerCommand = require('./commands.js').registerCommand;

module.exports = {
  inject: inject,
};

/**
 * Inject this plugin into the bot.
 *
 * @param {Bot} bot - The bot to inject into.
 */
function inject(bot) {
  /**
   * Tell a random joke.
   *
   * @param {string} username - The user that triggered the command.
   * @param {string[]} args - The given arguments.
   * @param {function} response - The response function.
   */
  function getJoke(username, args, response) {
    https.get(
        'https://official-joke-api.appspot.com/random_joke',
        (getResponse) => {
          let data = '';
          getResponse.on('data', (chunk) => {
            data += chunk;
          });
          getResponse.on('end', () => {
            const joke = JSON.parse(data);
            response(`${joke.setup} ${joke.punchline}`);
            log.debug(`Told the following joke: ${joke.setup} ${joke.punchline}`);
          });
        });
  }

  registerCommand('joke', getJoke, 'Get a random joke.', 'joke');
};
