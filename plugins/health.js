module.exports.inject = inject;

/**
 * Inject this plugin into the bot.
 *
 * @param {Bot} bot - The bot to inject into.
 */
function inject(bot) {
  const food = [
    'golden_carrot', 'cooked_mutton', 'cooked_porkchop',
    'cooked_salmon', 'cooked_beef', 'baked_potato',
    'beetroot_soup', 'beetroot', 'bread', 'carrot',
    'cooked_chicken', 'cooked_cod', 'cooked_rabbit',
    'mushroom_stew', 'rabbit_stew', 'apple',
    'dried_kelp', 'melon_slice', 'potato',
    'pumpkin_pie', 'beef', 'chicken', 'mutton',
    'porkchop', 'rabbit', 'cookie', 'honey_bottle',
    'cod', 'sweet_beries', 'salmon',
  ];
  let eating = false;
  let previousItem = null;

  const notify = (message) => {
    if (bot.options.quietMode) {
      bot.options.masters.forEach((master) => {
        if (master in bot.players) {
          bot.whisper(master, message);
        }
      });
    } else {
      bot.chat(message);
    }
  };

  /**
   * Search in inventory for food and eat it.
   *
   * @return {boolean} Whether the bot has any food to eat.
   */
  const eat = () => {
    if (eating || bot.food >= 20) {
      return;
    }
    eating = true;
    previousItem = bot.heldItem;
    const found = bot.inventory.items().find((item) => food.includes(item.name));
    if (found == undefined) {
      eating = false;
      notify('I\'m hungry but I don\'t have anything to eat!');
      log.warn(`The bot can't find anything to eat!`);
      return false;
    }

    bot.equip(found, 'hand');
    bot.consume((err) => {
      if (err) {
        throw err;
      } else {
        eating = false;
        if (bot.food < 20) {
          setTimeout(eat, 100);
        }
        bot.equip(previousItem, 'hand');
      }
    });
  };

  bot.on('entityHurt', (entity) => {
    if (entity == bot.entity) {
      notify(`Help, I'm taking damage!`);
      log.warn('The bot is taking damage!');
    }
  });

  bot.on('playerCollect', (collector, collected) => {
    if (bot.food < 20 && collector == bot.entity) {
      setTimeout(eat, 100);
    }
  });

  bot.on('health', () => {
    if (bot.food < 18) {
      eat();
    }
  });
};
