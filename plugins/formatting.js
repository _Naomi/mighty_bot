const registerCommand = require('./commands.js').registerCommand;

module.exports.inject = inject;

/**
 * Inject this plugin into the bot.
 *
 * @param {Bot} bot - The bot to inject into.
 */
function inject(bot) {
  registerCommand('formatting', (username, args, response) => response('0 Black, 1 DarkBlue, 2 DarkGreen, 3 DarkAqua, 4 DarkRed, 5 DarkPurple, 6 Gold, 7 Gray, 8 DarkGray, A Green, B Aqua, C Red, D Pink, E Yellow, F White, K Obfus, L Bold, M Strike, N Underline, O Ital, R Reset'), 'See formatting codes', 'formatting');
};
