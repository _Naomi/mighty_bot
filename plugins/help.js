const commands = require('./commands.js');

module.exports.inject = inject;

/**
 * Inject this plugin into the bot.
 *
 * @param {Bot} bot - The bot to inject into.
 */
function inject(bot) {
  /**
   * Help command.
   *
   * @param {string} username - The user that triggered the command.
   * @param {string[]} args - The given arguments.
   * @param {function} response - The response function.
   */
  function help(username, args, response) {
    const commandList = commands.getCommands();
    if (args.length == 0) {
      if (bot.checkPermission) {
        const allowedCommands = Object.keys(commandList).filter((command) => bot.checkPermission(username, command, response.source));
        response(`You're allowed: ` + allowedCommands.join(', '));
      } else {
        response('Commands: ' + Object.keys(commandList).join(', '));
      }
    } else if (Object.keys(commandList).includes(args[0].toLowerCase())) {
      response(`${args[0]}: ${commandList[args[0]].helpMsg}`);
      response(`Usage: ${commandList[args[0]].usageMsg}`);
    } else {
      response(`I could not find the command '${args[0]}'`);
    }
  }

  commands.registerCommand('help', help, 'Help menu for bot commands', 'help [command]');
};
