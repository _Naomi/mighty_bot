module.exports.inject = inject;

/**
 * Inject this plugin into the bot.
 *
 * @param {Bot} bot - The bot to inject into.
 */
function inject(bot) {
  let show = true;

  /**
   * Toggle all the skin parts.
   */
  function toggleSkin() {
    show = !show;
    bot.setSettings({
      skinParts: {
        showJacket: show,
        showHat: show,
        showLeftPants: show,
        showRightPants: show,
        showLeftSleeve: show,
        showRightSleeve: show,
      },
    });
  };

  setInterval(toggleSkin, 1000);
}
