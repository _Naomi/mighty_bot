const https = require('https');

const registerCommand = require('./commands.js').registerCommand;

module.exports = {
  inject: inject,
};

/**
 * Check the Minecraft wiki and return a promise with th result.
 *
 * @param {string} searchTerm - The thing to search for.
 * @return {Promise} Promise containing the result.
 */
function checkWiki(searchTerm) {
  return new Promise((resolve, reject) => {
    https.get(
        `https://minecraft.gamepedia.com/api.php?action=opensearch&format=json&formatversion=2&search=${searchTerm}&namespace=0&limit=1`,
        (response) => {
          let data = '';
          response.on('data', (chunk) => {
            data += chunk;
          });
          response.on('end', () => {
            resolve(JSON.parse(data)[3][0]);
          });
        });
  });
}

/**
 * Inject this plugin into the bot.
 *
 * @param {Bot} bot - The bot to inject into.
 */
function inject(bot) {
  /**
   * Check the Minecraft wiki.
   *
   * @param {string} username - The user that triggered the command.
   * @param {string[]} args - The given arguments.
   * @param {function} response - The response function.
   */
  function wiki(username, args, response) {
    checkWiki(args[0]).then((result) => {
      response(result);
      log.debug(`Found a result on the wiki for: ${args[0]}`);
    }).catch((err) => {
      response('I could not find anything, sorry :(');
    });
  }

  registerCommand('wiki', wiki, 'Check the Minecraft wiki', 'wiki search_term', 1);
};
