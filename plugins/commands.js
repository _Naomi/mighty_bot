const assert = require('assert');

module.exports = {
  inject: inject,
  registerCommand: registerCommand,
  getCommands: getCommands,
  options: {
    prefixEnabled: true,
    quietMode: false,
    shout: true,
  },
};

const commands = {};

/**
 * Inject this plugin into the bot.
 *
 * @param {Bot} bot - The bot to inject into.
 */
function inject(bot) {
  /**
   * Get the correct response function based on the source.
   *
   * @param {string} username - The username to respond to.
   * @param {string} source - The source of the command. (chat/whisper)
   * @param {boolean} shout - If true, always respond in chat.
   * @return {function} The correct response function.
   */
  function getResponseFunc(username, source, shout = false) {
    const response = (shout || (source == 'chat' && !bot.options.quietMode)) ? (message) => {
      bot.chat(message);
    } : (message) => {
      bot.chat(`/tell ${username} ` + message);
    };
    response.source = source;
    return response;
  }


  /**
   * Handle a received (potential) command.
   *
   * @param {string} username - The username that called the command.
   * @param {string} message - The command and all its parameters.
   * @param {string} source - The source of the command. (chat/whisper)
   */
  function handleCommand(username, message, source) {
    // Split the arguments at each space, except when it is in a string.
    const args = message.match(/(".*?"|[^"\s]+)+(?=\s*|\s*$)/g);
    const shout = bot.options.shout && args[0].endsWith('!');

    // Check whether there are any arguments.
    if (args == null) {
      return;
    }

    if (bot.options.prefixEnabled && source == 'chat') {
      // Check whether the message has the correct prefix.
      if (args[0].toLowerCase().startsWith(bot.entity.username.toLowerCase())) {
        if (args.length == 1) {
          return;
        }
        args.splice(0, 1); // Remove prefix if it did.
      } else {
        return; // And stop if it didn't.
      }
    }

    // If the argument is a string, remove the quotes.
    for (const i in args) {
      if (args[i].startsWith('"') && args[i].endsWith('"')) {
        args[i] = args[i].slice(1, -1);
      } else if (args[i].startsWith('"') || args[i].endsWith('"')) {
        getResponseFunc(username, source)('You have to enclose the whole argument with quotes.');
        return;
      }
    };
    if (Object.keys(commands).includes(args[0].toLowerCase())) {
      const response = getResponseFunc(username, source, shout);
      const command = commands[args[0].toLowerCase()];
      // Check if a plugin implemented some kind of permission system.
      if (bot.checkPermission) {
        if (!bot.checkPermission(username, args[0].toLowerCase(), source)) {
          response('You don\'t have the right permission.');
          return;
        }
      }
      if (command.minArgs+1 > args.length) {
        response(`Not enough arguments given, you need at least ${command.minArgs} arguments`);
        return;
      }
      log.info(`${username} called the command '${args[0]}' with the arguments: "${args.slice(1).join( )}"`);
      command.handler(username, args.slice(1), response);
    }
  }

  bot.on('chat:chat', (matches) => {
    handleCommand(matches[0][0], matches[0][1], 'chat');
  });

  bot.on('chat:whisper', (matches) => {
    log.debug(`${matches[0][0]} whispered: ${matches[0][1]}`);
    handleCommand(matches[0][0], matches[0][1], 'whisper');
  });
}

/**
 * Register a new command.
 *
 * @param {string} command - The name of the command.
 * @param {function} handler - The function to call.
 * @param {string} helpMsg - The help message.
 * @param {string} usageMsg - The usage message.
 * @param {interger} minArgs - The minimum number of arguments needed.
 */
function registerCommand(command,
    handler,
    helpMsg,
    usageMsg,
    minArgs = 0) {
  assert(
      typeof (command) === 'string',
      'Command \'' + command +
            '\' cannot be registered, invalid command name.');
  assert(
      typeof (handler) === 'function',
      'Command \'' + command + '\' cannot be registered, invalid handler.');
  assert(
      typeof (helpMsg) === 'string',
      'Command \'' + command + '\' cannot be registered, invalid help message.');
  assert(
      typeof (usageMsg) === 'string',
      'Command \'' + command + '\' cannot be registered, invalid usage message.');
  assert(
      typeof (minArgs) === 'number',
      'Command \'' + command +
            '\' cannot be registered, invalid minArgs value.');
  assert(
      !(command in commands),
      'Command \'' + command +
            '\' cannot be registered, command already exists.');

  commands[command] = {
    handler: handler,
    helpMsg: helpMsg,
    usageMsg: usageMsg,
    minArgs: minArgs,
  };
}

/**
 * Return all the registered commands.
 *
 * @return {Object.<string, object>} A map containing all registered commands.
 */
function getCommands() {
  return commands;
}
